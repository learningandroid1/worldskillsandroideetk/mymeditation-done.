package com.example.meditationport2.data.api

import com.example.meditationport2.data.model.FeelingsResponse
import com.example.meditationport2.data.model.LoginResponse
import com.example.meditationport2.data.model.QuotesResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiRetrofit {

   @POST("user/login")
   fun getAuth(
      @Header("Content-Type")type:String = "application/json",
      @Body hashMap: HashMap<String, String>
   ):Call<LoginResponse>

   @GET("quotes")
   fun getQuotes():Call<QuotesResponse>

   @GET("feelings")
   fun getFeelings():Call<FeelingsResponse>

}