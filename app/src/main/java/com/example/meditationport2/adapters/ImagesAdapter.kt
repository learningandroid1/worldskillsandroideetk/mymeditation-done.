package com.example.meditationport2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.RoundedCorner
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.meditationport2.NewImageClickListener
import com.example.meditationport2.R
import com.example.meditationport2.ui.profile.NewImageDialogFragment

class ImagesAdapter(val context: Context, val list: ArrayList<Int>, val click:NewImageClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val NewImage_ViewType = 0
    val Image_ViewType = 1

    class ImageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val image: ImageView = itemView.findViewById(R.id.image)
    }

    class NewImageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val button: LinearLayout = itemView.findViewById((R.id.new_image_linear))
    }

    override fun getItemViewType(position: Int): Int {
        return when (position){
            list.size -> NewImage_ViewType
            else -> Image_ViewType
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val root = LayoutInflater.from(context)
        return when(viewType){
            NewImage_ViewType-> NewImageViewHolder(root.inflate(R.layout.newimage_item,parent, false))
            else-> ImageViewHolder(root.inflate(R.layout.images_item,parent, false))
        }
       // LayoutInflater.from(context).inflate(R.layout.images_item,parent,false)
       // return ImageViewHolder(root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)){
            NewImage_ViewType-> {
                holder as NewImageViewHolder
                holder.button.setOnClickListener{
                  click.NewImage()

                  //  Toast.makeText(context, "Hello", Toast.LENGTH_SHORT).show()
                }
            }
        else->{
            holder as ImageViewHolder
            Glide.with(context).load(list[position]).transform(RoundedCorners(30)).into(holder.image)
            //holder.image.setImageResource(list[position])
        }
        }
    }

    override fun getItemCount(): Int {
        return list.size+1
    }
}
