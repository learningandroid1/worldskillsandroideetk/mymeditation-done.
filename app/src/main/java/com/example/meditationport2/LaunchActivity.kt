package com.example.meditationport2

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer

class LaunchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
        val sharedPreferences = getSharedPreferences("main", MODE_PRIVATE)
        val timer = object: CountDownTimer(10000,200){   // здесь мы только объявили и описали timer

            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {  // когда закончится отображение (оперделенное в таймере - переход на экран Onboarding
              val token = sharedPreferences.getString("token", null)
              if (token != null){
                  val intent = Intent(this@LaunchActivity,MenuActivity::class.java)
                  startActivity(intent)
              }
              else {
              val intent = Intent(this@LaunchActivity,OnboardingActivity::class.java)  // объявляем переменную "intent", в неё закладываем Intent, откуда куда и класс
              startActivity(intent)} // запускаем действие, конкретно наш intent

              finish()  // закрывает экран, чтобы он не висел. И чтобы не вернуться на него кнопкой бэк, ибо это трабл.
            }

        }
        timer.start()  // а здесь мы вызываем timer со всем, что в него встроено.
    }
}